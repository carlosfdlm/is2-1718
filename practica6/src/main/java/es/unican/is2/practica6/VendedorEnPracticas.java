package es.unican.is2.practica6;


public class VendedorEnPracticas extends Vendedor{
	
	/**
	 * Retorna un nuevo vendedor en practicas
	 * @param nombre
	 * @param dni
	 */
	public VendedorEnPracticas(String nombre, String dni) {
		super(nombre, dni, 0.0);
	}

}
